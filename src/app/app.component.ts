import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { AuthService } from './services/auth.service';
import { Router } from '@angular/router'; 
import { UsersService } from './services/bd/users.service';
import { User } from './shared/User';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  isLoggedIn= true; 
  photoUser = ''
  constructor(public authService: AuthService,private afAuth: AngularFireAuth, private userService: UsersService, private router: Router) { 
    this.afAuth.authState.subscribe(auth => {
      if (auth) {  
        this.isLoggedIn=true;
        this.infouser(auth.uid);
      } else {  
        this.isLoggedIn=false;
      }
    });
  }
  ngOnInit(): void {   
  }
  ////curren user
  async infouser(uid: string) {
    (await this.userService.getUser(uid)).subscribe((data) => { 
      data.map((e) => { 
        let f = e.payload.doc.data() as User; 
        this.photoUser= f.url 
      });
    });
  }
  home() { 
    this.router.navigate(['/'])
  }
  login() { 
    this.router.navigate(['/login'])
  }
  forgotpassword() { 
    this.router.navigate(['/forgot-password'])
  }
  books() {
    this.router.navigate(['/books'])
  } 
  mybooks() {
    this.router.navigate(['/mybooks'])
  }
  myaccount() {
    this.router.navigate(['/user-info'])
  }
  logout() {
    this.authService.signOut();
  }
}
