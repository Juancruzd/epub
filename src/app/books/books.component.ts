import { Component, OnInit } from '@angular/core';
import { BooksService } from 'src/app/services/bd/books.service';
import { Book } from 'src/app/shared/Book';
import { AuthService } from '../services/auth.service'; 
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  noresults = false;
  books!: Book[];
  bookscopy!: Book[];
  searchtext = '';
  constructor(private bookService: BooksService, public authService: AuthService) { }

  ngOnInit(): void {
    this.getbooks()
  }

  async getbooks() {
    (await this.bookService.getBooks()).subscribe(data => {
      this.books = data.map((e) => {
        const data = e.payload.doc.data() as Book;
        data.id = e.payload.doc.id
        return {
          ...data
        };
      });
      this.bookscopy = this.books;
    });
  }
  ///DELETE
  search(): void {
    if (this.searchtext == '' || this.searchtext.length <= 0) {
      this.books = this.bookscopy
    } else {
      this.books = this.books.filter(o => Object.keys(o).some(k => o.title.toLowerCase().includes(this.searchtext.toLowerCase()) || o.author.toLowerCase().includes(this.searchtext.toLowerCase()) || o.year.toLowerCase().includes(this.searchtext.toLowerCase())));

    }
    if (this.books.length <= 0) {
      this.noresults = true;
    } else {
      this.noresults = false;
    }
  }

}
