export class Book  {
    id!: string;
    key: string;
    name!: string;
    url!: string; 
    title: string;
    year: string;
    author: string; 
    constructor(key: string,title:string,year:string,author:string) { 
      this.title = title;
      this.year = year;
      this.author = author;
      this.key=key;
    }
}