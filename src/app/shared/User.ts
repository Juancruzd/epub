export class User {
    key!: string;
    name!: string;
    gender!: string;
    country!: string;
    
    email!: string; 

    photo!: string;
    url!: string; 

    constructor(key: string,name:string,gender:string,country:string) { 
        this.name = name;
        this.gender = gender;
        this.country = country;
        this.key=key;
      }
} 