import { Component, OnInit } from '@angular/core';
import { BookUploadService } from 'src/app/services/storage/book-upload.service';
import { Book } from 'src/app/shared/Book';
import { AngularFireAuth } from "@angular/fire/auth"; 
import Swal from 'sweetalert2';
@Component({
  selector: 'app-mybooks',
  templateUrl: './mybooks.component.html',
  styleUrls: ['./mybooks.component.css']
})
export class MybooksComponent implements OnInit {
  ///CREATE
  selectedFiles: FileList | any;
  currentBookUpload: Book | any;
  percentage: number | any;
  title = ''
  year = ''
  author = ''

  ////UPDATE
  selectedFilesU: FileList | any;
  currentBookUploadU: Book | any;
  percentageU: number | any;
  idDocU = ''
  titleU = ''
  yearU = ''
  authorU = ''
  nameU = ''
  urlU = ''
  //id usuario
  uid = ''

  ///BOOKS
  nobooks = false;
  books!: Book[];
 
  constructor(private bookuploadService: BookUploadService, private afAuth: AngularFireAuth) {
    this.afAuth.authState.subscribe(auth => {
      if (auth) {
        this.uid = auth.uid
        this.getbooks(this.uid)
      }
    });
  }

  ngOnInit(): void { 
  }

  ///GET
  async getbooks(uid: string) {
    (await this.bookuploadService.getBooksUser(uid)).subscribe((data) => {
      this.books = data.map((e) => {
        const data = e.payload.doc.data() as Book;
        data.id = e.payload.doc.id
        return {
          ...data
        };
      });
      if (this.books.length <= 0) {
        this.nobooks = true;
      } else {
        this.nobooks = false;
      }
    });
  }

  ////CREATE
  selectFile(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.selectedFiles = target.files as FileList;
  }

  uploadBook(): void {
    if (this.validateFormCreate()) {
      const file = this.selectedFiles.item(0);
      this.selectedFiles = undefined;
      this.currentBookUpload = new Book(this.uid, this.title, this.year, this.author);
      this.bookuploadService.saveBook(this.currentBookUpload, file).subscribe(
        (data) => {
          this.percentage = Math.round(data); 
        },
        (error) => { 
          Swal.fire({
            icon: 'error',
            title: 'Crear libro',
            text: error
          })
        }
      );
    }
  }

  ///DELETE
  deleteBook(book: Book, id: string): void {
    Swal.fire({
      title: 'Esta seguro de eliminar el libro'+book.title+' ?',
      showDenyButton: true,
      showCancelButton: false,
      confirmButtonText: `Eliminar`,
      denyButtonText: `Cancelar`,
    }).then((result) => { 
      if (result.isConfirmed) {
        this.bookuploadService.deleteBook(book, id);
        Swal.fire('Libro eliminado!', '', 'success')
      }  
    })
  }

  ////UPDATE
  selectFileU(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.selectedFilesU = target.files as FileList;
  }

  editBook(book: Book, id: string): void {
    this.percentageU = 0;
    this.idDocU = id
    this.titleU = book.title;
    this.yearU = book.year;
    this.authorU = book.author;
    this.nameU = book.name;
    this.urlU = book.url;
  }
  updateBook(): void {
    this.currentBookUploadU = new Book(this.uid, this.titleU, this.yearU, this.authorU);
    if (this.selectedFilesU) {
      if (this.validateFormUpdate()) {
        const file = this.selectedFilesU.item(0);
        this.selectedFilesU = undefined;
        this.bookuploadService.editBook(this.idDocU, this.nameU, this.currentBookUploadU, file).subscribe(
          (data) => {
            this.percentageU = Math.round(data);
          },
          (error) => {
            Swal.fire({
              icon: 'error',
              title: 'Editar libro',
              text: error
            })
          }
        );
      }
    } else {
      if (this.validateFormUpdate()) {
        this.bookuploadService.EditDataBook(this.currentBookUploadU, this.nameU, this.urlU, this.idDocU);
      }
    }
  }

  
  ///VALIDATE FORM
  validateFormCreate(): boolean {
    if (!this.selectedFiles) { 
      Swal.fire({
        icon: 'error',
        title: 'Subir libro',
        text:  'Seleccione un libro'
      })
      return false
    }
    if (this.title.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Subir libro',
        text:  'Ingrese el titulo del libro'
      })
      return false
    }
    if (this.year.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Subir libro',
        text:  'Ingrese el año del libro'
      })
      return false
    }
    if (this.author.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Subir libro',
        text:  'Ingrese el autor del libro'
      })
      return false
    } 
    return true
  }

  validateFormUpdate(): boolean {
    if (this.titleU.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Editar libro',
        text: 'Ingrese el titulo del libro'
      })
      return false
    }
    if (this.yearU.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Editar libro',
        text: 'Ingrese el año del libro'
      })
      return false
    }
    if (this.authorU.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'Editar libro',
        text:  'Ingrese el autor del libro'
      })
      return false
    } 
    return true
  }
}
