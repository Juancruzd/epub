import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MybooksRoutingModule } from './mybooks-routing.module';
import { MybooksComponent } from './mybooks.component';


@NgModule({
  declarations: [
    MybooksComponent
  ],
  imports: [
    CommonModule,
    MybooksRoutingModule
  ]
})
export class MybooksModule { }
