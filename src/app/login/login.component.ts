import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email = '';
  fieldpassword = false;
  password = '';  

  constructor(public authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
 
  onLoginEmail(): void {
    if (this.validateForm(this.email, this.password)) {
      this.authService.loginWithEmail(this.email, this.password)
        .then(() => this.router.navigate(['/mybooks']))
        .catch(_error => {
          Swal.fire(_error, '', 'error') 
        })
    }
  } 
   
  validateForm(email: string, password: string): boolean {
    if (password.length === 0 && email.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'INICIAR SESION',
        text: 'Ingrese el correo electrónico y la contraseña'
      })
      return false
    }

    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if ((email.length === 0) || (!EMAIL_REGEXP.test(email))) { 
      Swal.fire({
        icon: 'error',
        title: 'INICIAR SESION',
        text: 'Ingrese un correo electrónico valido'
      })
      return false
    } 

    if (password.length === 0) { 
      Swal.fire({
        icon: 'error',
        title: 'INICIAR SESION',
        text: 'Ingrese la contraseña'
      })
      return false
    }

    if (password.length < 6) { 
      Swal.fire({
        icon: 'error',
        title: 'INICIAR SESION',
        text: 'La contraseña debe contener al menos 6 caracteres'
      })
      return false
    } 
    return true
  } 
  
  ///
  toggleFieldPassword() {
    this.fieldpassword = !this.fieldpassword;
  }

  ///Enlaces
  register() { 
    this.router.navigate(['/register'])
  }
  forgotpassword() { 
    this.router.navigate(['/forgot-password'])
  }
}
