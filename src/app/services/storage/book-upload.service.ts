import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { BooksService } from '../bd/books.service'
import { Book } from '../../shared/Book';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class BookUploadService {
  private basePath = '/books';
  constructor(private storage: AngularFireStorage, private booksService: BooksService) { }

  saveBook(book: Book,file: File): Observable<number | any> {
    const filename= `book_${new Date().getTime()}_${file.name}`
    const filePath = `${this.basePath}/${filename}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath,file);

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          book.url = downloadURL;
          book.name =filename;
          this.saveBookData(book);
        });
      })
    ).subscribe();

    return uploadTask.percentageChanges();
  }

  private saveBookData(book: Book): void {   
    this.booksService.createBook(book)
      .then(async result => { 
        Swal.fire('Libro creado!', '', 'success')
      }).catch(error => { 
        Swal.fire(error, '', 'error')
      });
  }

  editBook(iddoc:string,name:string,book: Book,file: File): Observable<number | any> {
    const filename= `book_${new Date().getTime()}_${file.name}`
    const filePath = `${this.basePath}/${filename}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath,file);

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          this.deleteFileStorage(name); 
          this.EditDataBook(book,filename,downloadURL,iddoc);
        });
      })
    ).subscribe();

    return uploadTask.percentageChanges();
  }

  public EditDataBook(book: Book,name:string,url:string,iddoc:string): void {   
    this.booksService.updateBook(iddoc,name,url,book)
      .then(async result => {
        console.log(result);
        Swal.fire('Libro actualizado!', '', 'success')
      }).catch(error => { 
        Swal.fire(error, '', 'error')
      });
  } 
  
  getBooksUser(key: string) {
    return  this.booksService.getBooksByKey(key);
  }
  
  deleteBook(book: Book, id: string): void {
    this.booksService.deleteBook(id)
      .then(() => {
        this.deleteFileStorage(book.name);
      })
      .catch(error =>  Swal.fire(error, '', 'error'));
  }

  private deleteFileStorage(name: string): void {
    const storageRef = this.storage.ref(this.basePath);
    storageRef.child(name).delete();
  }
}