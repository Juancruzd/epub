import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';

import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { UsersService } from '../bd/users.service'
import { User } from '../../shared/User';

import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class UserUploadService {

  private basePath = '/users';
  constructor(private storage: AngularFireStorage, private usersService: UsersService) { }

  editUser(iddoc:string,photo:string,user: User,file: File): Observable<number | any> {
    const filename= `user_${new Date().getTime()}_${file.name}`;
    const filePath = `${this.basePath}/${filename}`;
    const storageRef = this.storage.ref(filePath);
    const uploadTask = this.storage.upload(filePath,file);

    uploadTask.snapshotChanges().pipe(
      finalize(() => {
        storageRef.getDownloadURL().subscribe(downloadURL => {
          if(photo){ 
            this.deleteFileStorage(photo); 
          }
          this.EditDataUser(user,filename,downloadURL,iddoc);
        });
      })
    ).subscribe();

    return uploadTask.percentageChanges();
  }

  public EditDataUser(user: User,photo:string,url:string,iddoc:string): void {    
    this.usersService.updateUser(iddoc,photo,url,user)
      .then(async result => { 
        Swal.fire('Información actualizada!', '', 'success')
      }).catch(error => { 
        Swal.fire(error, '', 'error')
      });
  }   

  private deleteFileStorage(name: string): void {
    const storageRef = this.storage.ref(this.basePath);
    storageRef.child(name).delete();
  }
}
