import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'; 
import { Book } from '../../shared/Book'; 
@Injectable({
  providedIn: 'root'
})
export class BooksService {
  constructor(private firestore: AngularFirestore) { }
  //Crea un nuevo libro
  public async createBook(book: Book) {   
    return this.firestore.collection('books').add({key: book.key , name: book.name, url: book.url, title: book.title,year: book.year, author:book.author});
  }
  //Obtiene todos los libros
  public async getBooks() {  
      return this.firestore.collection('books').snapshotChanges();  
  }
  //Obtiene libro
  public async getBook(key: string) {  
    return this.firestore.collection('books').doc(key).snapshotChanges();
  }
  //Obtiene todos los libro por usuario
  public async getBooksByKey(key: string) {
    return this.firestore.collection('books', ref => ref.where('key', '==', key)).snapshotChanges(); 
  }
  //Actualiza libro
  public async updateBook(documentId: string,name:string,url:string, book: Book) {
    return this.firestore.collection('books').doc(documentId).set({key: book.key , name: name, url: url, title: book.title,year: book.year, author:book.author});
  }
   //Borra libro
   public async deleteBook(documentId: string) {
    return this.firestore.collection('books').doc(documentId).delete();
  }
}
