import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'; 
import { User } from '../../shared/User'; 

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private firestore: AngularFirestore) { }
  //Crea un nuevo usuarios
  public async createUser(user: User) { 
    return this.firestore.collection('users').add({key: user.key , name: user.name,gender: user.gender,country: user.country, email:user.email});
  }
  //Obtiene usuario
  public async getUser(documentId: string) {  
     return this.firestore.collection('users', ref => ref.where('key', '==', documentId)).snapshotChanges(); 
  }
  //Obtiene todos los usuarios
  public async getUsers() {
    return this.firestore.collection('users').snapshotChanges();
  }
  //Actualiza usuario
  public async updateUser(documentId: string,photo:string,url:string,user: User) {
    return this.firestore.collection('users').doc(documentId).set({key: user.key , photo: photo, url: url, name: user.name,gender: user.gender, country:user.country});
  }

}
