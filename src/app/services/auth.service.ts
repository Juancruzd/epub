import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router'; 
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: any = null;

  constructor(private afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.authState.subscribe(auth => {
      if (auth) { 
        this.authState = auth
      } else {  
      }
    })
  }

  get isUserAnonymousLoggedIn(): boolean {
    return (this.authState !== null) ? this.authState.isAnonymous : false
  }

  get currentUserId(): string {
    return (this.authState !== null) ? this.authState.uid : ''
  }

  get currentUserName(): string {
    return this.authState['email']
  }

  get currentUser(): any {
    return (this.authState !== null) ? this.authState : null;
  }
  
  detailsuser(){ 
    this.afAuth.authState.subscribe(auth  =>{
        if (auth) { 
          return auth;
       }
       else{
        return "";
       }
      }); 
  }

  get isUserEmailLoggedIn(): boolean {
    if ((this.authState !== null) && (!this.isUserAnonymousLoggedIn)) {
      return true
    } else {
      return false
    }
  } 
  get isLoggedIn(): Promise<boolean> {
    return new Promise((resolve: any) => {
      this.afAuth.onAuthStateChanged(( user: any ) => {
        user ? resolve(true) : resolve(false);
      });
    });
  }
  
  async signUpWithEmail(email: string, password: string) {
    return new Promise<any>((resolve, reject) => {
      this.afAuth.createUserWithEmailAndPassword(email,password)
      .then(res => {
        resolve(res);
        this.authState = res
      }, err => reject(err))
    }) 
  }

  loginWithEmail(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((user) => {
        this.authState = user
      })
      .catch(error => {
        console.log(error)
        throw error
      });
  }

  signOut(): void {
    this.afAuth.signOut();
    this.router.navigate(['/'])
  }
}