import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';
import { AngularFireAuth } from "@angular/fire/auth";
import { UsersService } from '../services/bd/users.service';
import { ApiService } from '../services/api.service'
import { UserUploadService } from 'src/app/services/storage/user-upload.service';
import { User } from '../shared/User';
import { Country } from '../shared/Country';
import { Gender } from '../shared/Gender';
@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {
  ////UPDATE
  selectedFilesU: FileList | any;
  currentUserUploadU: User | any;
  percentageU: number | any;
  idDocU = '' 
  photoU = ''
  urlU = ''
  //id usuario
  uid = '' 
 
  key = '';
  name = '';
  countries: Country[] = [];
  country = '';
  genders: Gender[] = [{
    name: 'Femenino'
  }, {
    name: 'Masculino'
  }];
  gender = '';
  email = '';
 

  constructor(public authService: AuthService, private userService: UsersService,private useruploadService: UserUploadService, private router: Router, public api: ApiService,private afAuth: AngularFireAuth) { 
    this.afAuth.authState.subscribe(auth => {
      if (auth) {   
        this.uid = auth.uid  
        this.infouser(this.uid);
      }  
    });
  }

  ngOnInit(): void {
    ///paises
    this.fetchCountries();
  } 
  ////curren user
  async infouser(uid: string) {
    (await this.userService.getUser(uid)).subscribe((data) => { 
      data.map((e) => {
        this.idDocU = e.payload.doc.id
        let f = e.payload.doc.data() as User;
        this.key = f.key
        this.name = f.name
        this.gender = f.gender
        this.country = f.country
        this.email = f.email

        this.photoU= f.photo
        this.urlU=f.url 
      });
    });
  }
  selectFileU(event: Event): void {
    const target = event.target as HTMLInputElement;
    this.selectedFilesU = target.files as FileList;
  }
  async onUpdate(): Promise<void> { 
    this.currentUserUploadU =  new User(
      this.key,
      this.name,
      this.gender,
      this.country
    );
    if (this.selectedFilesU) {
      if (this.validateForm()) {
        const file = this.selectedFilesU.item(0);
        this.selectedFilesU = undefined;
        this.useruploadService.editUser(this.idDocU, this.photoU, this.currentUserUploadU, file).subscribe(
          (data) => {
            this.percentageU = Math.round(data);
          },
          (error) => {
            Swal.fire({
              icon: 'error',
              title: 'Editar información',
              text: error
            })
          }
        );
      }
    } else {
      if (this.validateForm()) {
        if(!this.photoU){
          this.photoU = ""
          this.urlU = ""
        }
        this.useruploadService.EditDataUser(this.currentUserUploadU,this.photoU, this.urlU, this.idDocU);
      }
    }
  }
  validateForm(): boolean {  
    if (this.name == "") { 
      Swal.fire('Ingrese su nombre', '', 'error')
      return false;
    }
    if (this.country == "") { 
      Swal.fire('Ingrese su pais', '', 'error')
      return false;
    } 
    if (this.gender == "") { 
      Swal.fire('Ingrese su genero', '', 'error')
      return false;
    } 
    return true;
  }


  ////funcion que obtiene los paises  a mostrar en el select
  async fetchCountries() {
    this.api.getcountries().subscribe(
      (data) => {
        this.countries = data as Country[];
        this.countries.sort((t1, t2) => {
          const name1 = t1.name.toLowerCase();
          const name2 = t2.name.toLowerCase();
          if (name1 > name2) { return 1; }
          if (name1 < name2) { return -1; }
          return 0;
        });
      }, (error) => {  Swal.fire(error, '', 'error') });
  };
}
