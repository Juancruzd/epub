import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { UsersService } from '../services/bd/users.service';
import { ApiService } from '../services/api.service';
import { User } from '../shared/User';
import { Country } from '../shared/Country';
import { Gender } from '../shared/Gender';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  name = '';
  countries: Country[] = [];
  country = '';
  genders: Gender[] = [{
    name: 'Femenino'
  }, {
    name: 'Masculino'
  }];
  gender = '';
  email = '';
  fieldpassword = false;
  password = '';
  
  constructor(public authService: AuthService, private router: Router, public api: ApiService,private userService: UsersService) { }

  ngOnInit() {
    ///paises
    this.fetchCountries();
  }

  ////funcion que obtiene los paises  a mostrar en el select
  async fetchCountries() {
    this.api.getcountries().subscribe(
      (data) => {
        this.countries = data as Country[];
        this.countries.sort((t1, t2) => {
          const name1 = t1.name.toLowerCase();
          const name2 = t2.name.toLowerCase();
          if (name1 > name2) { return 1; }
          if (name1 < name2) { return -1; }
          return 0;
        });
      }, (error) => { Swal.fire(error, '', 'error') });
  };



  onSignUp(): void { 
    if (this.validateForm(this.email, this.password)) { 
      this.authService.signUpWithEmail(this.email, this.password)
        .then(async (result) => {  
          let user=new User(result.user.uid, this.name,this.gender,this.country); 
          user.email=this.email;
          await this.userService.createUser(user).then(async result => { 
            this.router.navigate(['/mybooks']);
          });
        }).catch(_error => { 
          Swal.fire(_error, '', 'error')
        })
    } 
  }

  validateForm(email: string, password: string): boolean { 
    
    if (this.name == "") {  
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese su nombre'
      })
      return false;
    }
    if (this.gender == "") {  
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese su genero'
      })
      return false;
    } 
    if (this.country == "") {  
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese su pais'
      })
      return false;
    } 
    if (password.length === 0 && email.length === 0) {  
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese el correo electrónico y la contraseña'
      })
      return false
    }

    const EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    if ((email.length === 0) || (!EMAIL_REGEXP.test(email))) { 
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese un correo electrónico valido'
      })
      return false
    } 

    if (password.length === 0) {  
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'Ingrese la contraseña'
      })
      return false
    }

    if (password.length < 6) {   
      Swal.fire({
        icon: 'error',
        title: 'CREAR CUENTA',
        text: 'La contraseña debe contener al menos 6 caracteres'
      })
      return false
    } 
    return true;
  }
  toggleFieldPassword() {
    this.fieldpassword = !this.fieldpassword;
  }
  login() {
    this.router.navigate(['/login'])
  }
}
